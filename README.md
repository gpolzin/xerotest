## Instructions

Hello and welcome to the Xero Front-End Developer pre-interview exercise!

To get a better idea of your skills, we'd like you to implement a basic invoicing UI based on a wireframe design.

You should spend no more than one hour on this exercise, allowing 10-15 minutes for a write-up at the end. We don't expect you to complete everything, but we'd like you to write down (at the bottom of this file) a high-level overview of what else you would do to complete the task, and then what else you might do to to get the code to a production-grade state.

This repository was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). 

If you are not comfortable with React, please feel free to start from scratch with something you are comfortable with, be it a different framework, or vanilla JavaScript + HTML + CSS.

A junior developer started writing some code in `src/App.js`. Feel free to use or adapt that if it's useful.

We want to see code that speaks to your strengths. If you feel your strengths are in design and CSS, then please feel free to spend the time on making the form more beautiful than the wireframe. If your strengths are in JavaScript, then feel free to focus on the logical aspects of the functionality.

## The UI and functionality we'd like you to build

![Invoice wireframe](public/frontend_peer_programming_interview_720.png)

The design consists of three input fields and a button laid out horizontally. The input fields are: Description, Cost and Quantity. 

The button text is "Add item", and when clicked, it should add a new row to a table below the input fields. 

The table has four columns: Description, Cost, Quantity, and Price, where the Price is the Cost of the item multiplied by the Quantity.

Below the table is a right-aligned total that contains the sum of all prices in the table.

Below the total is a right-aligned button with the text "Submit invoice". When clicked, this would normally make a call to a backend, but for this exercise, you can just make a call to `console.log` with the data you would send to a backend API.

We look forward to seeing what you come up with!

## Your write-up

Tell us what you'd do next if you had more time, and what else you might do to to get the code to a production-grade state.

### Geoff Polzin - Xero Code Test

Thanks for reviewing my Xero Code Test. The project has been generated with Create React App and Typescript. To get started, run `npm install` then `npm start`. Run `npm test` to run the tests.

I had a little bit of extra time over the weekend so I'll admit I did extend my solution a little but I have included a full git history, tagged for your convenience with some major milestones so you can view the solution at it's base level and some of the enhancements I added with extra time. The tags are as follows:

* `feature-complete` - The solution is feature complete as per the brief (with the exception of layout) sticking to a shorter time frame.
* `styled-and-tested` - The solution was given some basic styling and polished up a bit. I also added some basic testing.
* `redux-state-option` - Although complete overkill for this project, I wanted to lift the state into a Redux store and demonstrate how this could be done while performing minimal modifications to the base code. I also added additional tests for the reducer and selectors.

### For Production Grade
There are many additions that I would make to make the code production quality, both relating to features/UI and code quality:

#### Code Quality
* more robust and testable form validation
* implement a React error boundary to catch any unexpected logic errors and recover gracefully
* extract typescript models out into their own modules
* more comprehensive unit testing and/or E2E testing
* implement `add-purchase-form.tsx` form state into it's own custom React Hook for managing form state rather than using individual `useState()` Hooks.
* wrap `invoice-creation-page.tsx` state into a custom Hook so that implementation details of whether state is in Redux or internal state is interchangable and not known to the component.
* utilise CSS modules for namespaced styling
* implement a CSS framework like Tailwind or something heavier like Material or Bootstrap to provide richer styling
* error logging to a telemetery provider ie DataDog or AppInsights

#### Features
* allow removal of line items
* allow editing of line items
* read-only preview of invoice before submission
* submit invoice payload to API via Axios or Fetch
* provide a calculated breakdown of sales tax in `purchase-summary.tsx`
