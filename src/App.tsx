import React from 'react';
import './App.css';
import { InvoiceCreationPage } from './invoices/invoice-creation-page';

function App() {
  return (
    <InvoiceCreationPage />
  );
}

export default App;
