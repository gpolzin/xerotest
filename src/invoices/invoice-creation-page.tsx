import * as React from 'react';
import { PurchaseList } from './components/purchase-list';
import { PurchaseSummary } from './components/purchase-summary';
import { AddPurchaseForm } from './components/add-purchase-form';
import { ILineItem, LineItemViewModel } from './components/models';
import './invoice-creation-page.css';
import { ActionPanel } from './components/action-panel';
import { useDispatch, useSelector } from 'react-redux';
import { duckOperations, duckSelectors } from './duck';
import { IAppState } from '../store/store';

export const InvoiceCreationPage: React.FC = () => {
    const dispatch = useDispatch();
    const lineItems = useSelector<IAppState, LineItemViewModel[]>((state: IAppState) => duckSelectors.lineItemModelsSelector(state));
    const grandTotal = useSelector<IAppState, number>((state: IAppState) => duckSelectors.grandTotalSelector(state));

    const onAddItem = (lineItem: ILineItem) => {
        dispatch(duckOperations.addLineItem(lineItem));
    };

    const onDeleteItem = (index: number) => {
        dispatch(duckOperations.deleteLineItem(index));
    }

    return (
        <div className="page-wrapper">
            <h1>Invoice</h1>
            <AddPurchaseForm onAddItem={onAddItem} />
            <PurchaseList lineItems={lineItems} onDelete={onDeleteItem} />
            <PurchaseSummary totalCost={grandTotal} />
            <ActionPanel>
                <button onClick={() => console.table(lineItems)}>Submit invoice</button>
            </ActionPanel>
        </div>
    );
}