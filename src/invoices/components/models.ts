export interface ILineItem {
    description: string;
    cost: number;
    quantity: number;
}

export type LineItemViewModel = ILineItem & { subTotal: number }