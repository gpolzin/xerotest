import { shallow } from "enzyme";

import * as React from 'react';
import { LineItemViewModel } from "./models";
import { PurchaseListItem } from "./purchase-list-item";

describe("Purchase list", () => {
    test("Should match snapshot", () => {
        const lineItem: LineItemViewModel =
        {
            description: "item 1",
            cost: 2,
            quantity: 1,
            subTotal: 20
        };

        const component = shallow(<PurchaseListItem lineItem={lineItem} />);
        expect(component).toMatchSnapshot();
    })
});