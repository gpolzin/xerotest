import * as React from 'react';
import { ILineItem } from './models';
import './add-purchase-form.css';

interface Props {
    onAddItem: (lineItem: ILineItem) => void;
}

export const AddPurchaseForm: React.FC<Props> = (props: Props) => {
    const [description, setDescription] = React.useState('');
    const [cost, setCost] = React.useState('');
    const [quantity, setQuantity] = React.useState('');

    const descriptionRef = React.useRef<HTMLInputElement>(null);

    const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        props.onAddItem({
            description,
            cost: parseFloat(cost) || 0,
            quantity: parseInt(quantity) || 0
        });

        resetForm();
    }

    const resetForm = () => {
        setDescription('');
        setCost('');
        setQuantity('');

        descriptionRef.current!.focus();
    }

    return (
        <form className="add-purchase-form" onSubmit={onSubmit}>
            <div className="field-group">
                <label htmlFor="description">Description</label>
                <input type="text" ref={descriptionRef} id="description" name="description" required onChange={(e) => setDescription(e.target.value)} value={description} />
            </div>

            <div className="field-group num">
                <label htmlFor="cost">Cost</label>
                <input type="number" step=".01" id="cost" name="cost" required min="1" onChange={(e) => setCost(e.target.value)} value={cost} />
            </div>

            <div className="field-group num">
                <label htmlFor="quantity">Quantity</label>
                <input type="number" id="quantity" name="quantity" required onChange={(e) => setQuantity(e.target.value)} value={quantity} />
            </div>
            <button type="submit">Add item</button>
        </form>
    );
}