import * as React from 'react';
import { PurchaseListItem } from './purchase-list-item';
import { LineItemViewModel } from './models';
import './purchase-list.css';

interface Props {
    lineItems: LineItemViewModel[];
    onDelete: (index: number) => void;
}

export const PurchaseList: React.FC<Props> = (props: Props) => {
    return (
        <React.Fragment>
            { !(!!props.lineItems.length) && 
            <p>No invoice line items have been added yet.</p>
            }
            { !!props.lineItems.length &&
            <table className="purchase-list" cellSpacing="0">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th className="num">Cost</th>
                        <th className="num">Quantity</th>
                        <th className="num">Price</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {props.lineItems.map((lineItem, index) => <PurchaseListItem onDelete={() => props.onDelete(index)} key={index} lineItem={lineItem} />)}
                </tbody>
            </table>
            }
        </React.Fragment>
    );
}