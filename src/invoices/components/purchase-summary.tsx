import * as React from 'react';
import "./purchase-summary.css";

interface Props {
    totalCost: number;
}

export const PurchaseSummary: React.FC<Props> = (props: Props) => {
    return (
        <ul className="purchase-summary">
            <li>
                Total: {new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'AUD' }).format(props.totalCost)}
            </li>
        </ul>
    );
}