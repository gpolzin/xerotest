import * as React from 'react';
import { LineItemViewModel } from './models';

interface Props {
    lineItem: LineItemViewModel;
    onDelete: () => void;
}

export const PurchaseListItem: React.FC<Props> = (props: Props) => {
    const {lineItem} = props;

    const handleDelete = () => {
        if (window.confirm("Are you sure you want to delete this?")) {
            props.onDelete();
        }
    }

    return (
        <tr>
            <td>{lineItem.description}</td>
            <td className="num">${lineItem.cost.toFixed(2)}</td>
            <td className="num">{lineItem.quantity}</td>
            <td className="num">{ new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'AUD' }).format(lineItem.subTotal) }</td>
            <td><button onClick={handleDelete}>Delete</button></td>
        </tr>
    );
}