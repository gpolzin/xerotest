import { shallow } from "enzyme";

import * as React from 'react';
import { PurchaseList } from "./purchase-list";
import { LineItemViewModel } from "./models";

describe("Purchase list", () => {
    test("should match snapshot", () => {
        const lineItems: LineItemViewModel[] = [
            {
                description: "item 1",
                cost: 2,
                quantity: 1,
                subTotal: 20
            },
            {
                description: "item 2",
                cost: 1,
                quantity: 2,
                subTotal: 10
            }
        ];
        const component = shallow(<PurchaseList lineItems={lineItems} />);
        expect(component).toMatchSnapshot();
    });
});