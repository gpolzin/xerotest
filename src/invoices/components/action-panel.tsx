import * as React from 'react';
import './action-panel.css';

export const ActionPanel: React.FC = (props) => {
    return (
        <div className="action-panel">
            { props.children }
        </div>
    );
}