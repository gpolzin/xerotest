import * as React from 'react';
import { AddPurchaseForm } from './add-purchase-form';
import { ILineItem } from './models';
import { mount } from 'enzyme';

describe("Add purchase form", () => {
    test("Should match snapshot", () => {
        const addItem = jest.fn();
        const component = mount(<AddPurchaseForm onAddItem={addItem} />);

        expect(component).toMatchSnapshot();
    });

    test("It should execute onAddItem with a new line item object", () => {
        const addItem = jest.fn();
        const component = mount(<AddPurchaseForm onAddItem={addItem} />);

        const form = component.find("form").first();
        const descriptionField = component.find("input[name='description']").first();
        const costField = component.find("input[name='cost']").first();
        const quantityField = component.find("input[name='quantity']").first();

        const testDescription = "my description";
        const testCost = 123;
        const testQuantity = 321;
        descriptionField.simulate('change', { target: { value: testDescription } });
        costField.simulate('change', { target: { value: testCost } });
        quantityField.simulate('change', { target: { value: testQuantity } });

        form.simulate('submit');

        const expected: ILineItem = { description: testDescription, cost: testCost, quantity: testQuantity };

        expect(addItem).toHaveBeenCalledWith(expected);
    });

});