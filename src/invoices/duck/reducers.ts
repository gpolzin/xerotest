import * as fromActions from "./actions";
import { ILineItem } from "../components/models";

export interface IInvoicesState {
    lineItems: ILineItem[],
}

export const invoicesInitialState: IInvoicesState = {
    lineItems: [],
}

export default function invoicesReducer(state: IInvoicesState = invoicesInitialState, action: fromActions.Actions): IInvoicesState {
    switch (action.type) {
        case fromActions.ActionTypes.ADD_LINE_ITEM:
            return {
                ...state,
                lineItems: [...state.lineItems, action.payload],
            }
        case fromActions.ActionTypes.DELETE_LINE_ITEM:
            return {
                ...state,
                lineItems: state.lineItems.filter((item, index) => index !== action.payload),
            }
        default: {
            return state;
        }
    }
}