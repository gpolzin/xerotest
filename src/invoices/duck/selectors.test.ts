import { duckSelectors } from ".";
import { ILineItem } from "../components/models";

describe("Invoices selectors", () => {
    const lineItems: ILineItem[] = [
        {
            description: "Item 1",
            cost: 20,
            quantity: 2,
        },
        {
            description: "Item 2",
            cost: 10,
            quantity: 3,
        }
    ];
    test("should produce the subTotal for each item", () => {
        const result = duckSelectors.lineItemModelsSelector.resultFunc(lineItems);
        expect(result[0].subTotal).toBe(40);
        expect(result[1].subTotal).toBe(30);
    });

    test("should produce the grand total of all items", () => {
        const result = duckSelectors.grandTotalSelector.resultFunc(lineItems);
        expect(result).toBe(70);
    });
});