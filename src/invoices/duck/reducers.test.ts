import invoicesReducer, { duckOperations } from ".";
import { IAppState } from "../../store/store";
import { ILineItem } from "../components/models";
import { IInvoicesState } from "./reducers";

describe("Invoices reducer", () => {
    it("Should add the new item to the end of the lineItems array", () => {
        const initialState: IInvoicesState = {
            lineItems: [
                {
                    description: "item 1",
                    cost: 2,
                    quantity: 1
                },
                {
                    description: "item 2",
                    cost: 1,
                    quantity: 2
                }
            ],
        };

        const newItem = { description: "item 3", cost: 20, quantity: 3 };
        const resultState = invoicesReducer(initialState, duckOperations.addLineItem(newItem));
        expect(resultState.lineItems.length).toBe(3);
        expect(resultState.lineItems[2]).toEqual(newItem);
    });

    it("Should delete an item by index", () => {
        const initialState: IInvoicesState = {
            lineItems: [
                {
                    description: "item 1",
                    cost: 2,
                    quantity: 1
                },
                {
                    description: "item 2",
                    cost: 1,
                    quantity: 2
                },
                {
                    description: "item 3",
                    cost: 3,
                    quantity: 1
                }
            ],
        };

        const itemToRemove = 1;

        const resultState = invoicesReducer(initialState, duckOperations.deleteLineItem(itemToRemove));
        expect(resultState.lineItems.length).toBe(2);
        expect(resultState.lineItems.find((item) => item.description === "item 2")).not.toBeDefined();
    });
})