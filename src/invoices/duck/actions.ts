import { ActionUnion } from "../../store/action-union";
import { createAction } from "../../store/action-helpers";
import { ILineItem } from "../components/models";

export enum ActionTypes {
    ADD_LINE_ITEM = "[Invoices] Add Line Item",
    DELETE_LINE_ITEM = "[Invoices] Delete Line Item",
}

export const Actions = {
    addLineItem: (lineItem: ILineItem) => createAction(ActionTypes.ADD_LINE_ITEM, lineItem),
    deleteLineItem: (index: number) => createAction(ActionTypes.DELETE_LINE_ITEM, index),
};

export type Actions = ActionUnion<typeof Actions>;