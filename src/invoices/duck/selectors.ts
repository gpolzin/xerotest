import { IAppState } from "../../store/store";
import { createSelector } from "reselect";
import { ILineItem, LineItemViewModel } from "../components/models";

const lineItemsSelector = (state: IAppState) => state.invoices.lineItems;

const lineItemModelsSelector = createSelector(
    lineItemsSelector,
    (lineItems: ILineItem[]) => {
        return lineItems.map((item) => {
            const model: LineItemViewModel = {
                description: item.description,
                cost: item.cost,
                quantity: item.quantity,
                subTotal: item.cost * item.quantity
            }

            return model;
        });
    }
);

const grandTotalSelector = createSelector(
    lineItemsSelector,
    (lineItems: ILineItem[]) => {
        return lineItems.reduce((accumulator, current) => (accumulator + (current.cost * current.quantity)), 0);
    }
);

export default {
    lineItemModelsSelector,
    grandTotalSelector,
}