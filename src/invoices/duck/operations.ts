import { Actions } from "./actions";

const addLineItem = Actions.addLineItem;
const deleteLineItem = Actions.deleteLineItem;

export default {
    addLineItem,
    deleteLineItem,
}