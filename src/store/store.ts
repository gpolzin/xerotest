import { compose, combineReducers, createStore } from "redux";
import checkoutReducer, { IInvoicesState } from "../invoices/duck/reducers";

export interface IAppState {
    invoices: IInvoicesState;
}

export default function configureStore() {
    // Enable support for Redux devtools
    const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const rootReducer = combineReducers({
        invoices: checkoutReducer,
    });

    return createStore(
        rootReducer,
        composeEnhancers(),
    );
}