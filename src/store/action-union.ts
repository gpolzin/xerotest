// Type safe functional action creator implementation borrowed from
// https://medium.com/@martin_hotell/improved-redux-type-safety-with-typescript-2-8-2c11a8062575

type FunctionType = (...args: any[]) => any;
interface IActionCreatorsMapObject { [actionCreator: string]: FunctionType; }

export type ActionUnion<A extends IActionCreatorsMapObject> = ReturnType<A[keyof A]>;
